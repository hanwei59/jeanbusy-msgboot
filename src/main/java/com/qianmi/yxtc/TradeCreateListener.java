package com.qianmi.yxtc;

import com.alibaba.fastjson.JSON;
import com.qianmi.base.response.CentreResponse;
import com.qianmi.yxtc.exceptions.TradeException;
import com.qianmi.yxtc.trade.domain.ItemOrder;
import com.qianmi.yxtc.trade.domain.TradeOrder;
import com.qianmi.yxtc.trade.query.api.TradingQueryProvider;
import com.qianmi.yxtc.trade.query.request.TradeDetailQueryRequest;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import javax.jms.*;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author hanwei 15/8/21
 */
@Component("TradeCreateListener")
public class TradeCreateListener implements MessageListener {
    public TradeCreateListener() {
        System.out.println("TradeCreateListener");
    }

    @Autowired
    RedisTemplate stringRedisTemplate;

    @Autowired
    TradingQueryProvider tradingQueryProvider;

    public void onMessage(Message message) {
        String msg = "";
        try {
            if (message instanceof TextMessage) {
                TextMessage tmsg = (TextMessage) message;
                msg = tmsg.getText();
            } else if (message instanceof ObjectMessage) {
                ObjectMessage tmsg = (ObjectMessage) message;
            }
        } catch (JMSException e) {
            e.printStackTrace();
        }
        System.out.println("收到消息："+msg);
        Map<String, String> mapMessage=(Map<String, String>) JSON.parseObject(msg, Map.class);
        String tid = mapMessage.get("tid");
        String adminUserId = mapMessage.get("adminUserId");
        String picUrl = "";
        String itemName = "";

        TradeDetailQueryRequest request = new TradeDetailQueryRequest();
        request.setTid(tid);
        request.setFromIp("127.0.0.1");
        request.setDtype("10");
        request.setRequestUsercode(adminUserId);
        try {
            CentreResponse<TradeOrder> response = tradingQueryProvider.queryTradeDetail(request);
            if(response != null && response.getData() != null){
                List<ItemOrder> orderList = response.getData().getItemOrders();
                picUrl = orderList.get(0).getPicUrl();
                if(StringUtils.isNotBlank(picUrl)){
                    picUrl = "http://www.qmimg.com/"+picUrl;
                }
                itemName = URLEncoder.encode(orderList.get(0).getItemName());
            }
        } catch (TradeException e) {
            e.printStackTrace();
        }

        Map<String,String> obj = new HashMap<String, String>();
        obj.put("tid", tid);
        obj.put("url",picUrl);
        obj.put("gname", itemName);
        String string = JSON.toJSONString(obj);
        System.out.println("发送准备消息到redis：" + string);
        stringRedisTemplate.convertAndSend("channel", string);

        System.out.println("发送已消息到redis：" + string);
    }
}

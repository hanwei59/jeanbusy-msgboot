package com.qianmi.yxtc;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.command.ActiveMQTopic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.jms.listener.DefaultMessageListenerContainer;

/**
 * @author hanwei 15/8/21
 */
@Configuration
@ImportResource({"classpath*:appContext-dubbo.xml",
        "classpath*:appContext-resources.xml",
        "classpath*:META-INF/qianmi/dubbo/consumer/*.xml"})
public class AppConfig {

    @Autowired
    TradeCreateListener tradeCreateListener;

    @Bean
    public DefaultMessageListenerContainer tradeListenerContainer(){
        DefaultMessageListenerContainer container = new DefaultMessageListenerContainer();
        container.setMessageListener(tradeCreateListener);
        ActiveMQConnectionFactory activeMQConnectionFactory = new ActiveMQConnectionFactory();
//        activeMQConnectionFactory.setBrokerURL("tcp://172.19.66.39:61616");
        activeMQConnectionFactory.setBrokerURL("tcp://192.168.65.182:61616");
        container.setConnectionFactory(activeMQConnectionFactory);
        ActiveMQTopic topic = new ActiveMQTopic("t.yxtc.OperateOrder");
        container.setDestination(topic);
        return container;
    }

    @Bean
    public RedisTemplate stringRedisTemplate(){
        RedisTemplate stringRedisTemplate = new RedisTemplate();
        JedisConnectionFactory connectionFactory = new JedisConnectionFactory();
//        connectionFactory.setHostName("172.19.67.131");
        connectionFactory.setHostName("127.0.0.1");
        connectionFactory.setPort(6379);
        connectionFactory.setDatabase(1);
        connectionFactory.setUsePool(false);
//        JedisPoolConfig poolConfig = new JedisPoolConfig();
//        connectionFactory.setPoolConfig(poolConfig);
        stringRedisTemplate.setConnectionFactory(connectionFactory);
        stringRedisTemplate.setKeySerializer(new StringRedisSerializer());
        stringRedisTemplate.setValueSerializer(new StringRedisSerializer());
        stringRedisTemplate.setHashKeySerializer(new StringRedisSerializer());
        stringRedisTemplate.setHashValueSerializer(new StringRedisSerializer());
        return stringRedisTemplate;
    }
}

package bak;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.command.ActiveMQTopic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jms.listener.DefaultMessageListenerContainer;

/**
 * @author hanwei 15/8/21
 */
public class TradeListenerContainer extends DefaultMessageListenerContainer {

    @Autowired
    @Qualifier("TradeCreateListener")
    @Override
    public void setMessageListener(Object messageListener) {
        super.setMessageListener(messageListener);
        ActiveMQConnectionFactory activeMQConnectionFactory = new ActiveMQConnectionFactory();
        activeMQConnectionFactory.setBrokerURL("tcp://172.19.66.39:61616");
        super.setConnectionFactory(activeMQConnectionFactory);
        ActiveMQTopic topic = new ActiveMQTopic("t.yxtc.OperateOrder");
        super.setDestination(topic);
    }

}

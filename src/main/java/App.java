import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author hanwei 15/8/21
 */

@ComponentScan("com.qianmi.yxtc")
@EnableAutoConfiguration
public class App {


    public static void main(String[] args) throws Exception {
        SpringApplication.run(App.class, args);
    }
}
